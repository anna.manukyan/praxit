#include <string>

class MiniComputer
{
public:
    MiniComputer();
    int run();

private:
    void printProgramNameAndVersion();
    void printMainMenu();
    void printPrintMenu();
    void printMainMenuItem(int index);
    void printPrintMenuItem(int index);
    int getCommandFromUser();

    void executeCommand(int command);
    void executeExitCommand();
    void executeLoadCommand();
    void executePrintCommand();
    void executePrintRegister();
    void executePrintString();

private:
    std::string programName_;
    std::string version_;
};
