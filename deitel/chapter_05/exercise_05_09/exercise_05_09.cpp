#include <iostream>

int
main()
{
    int numbers = 1;

    for (int counter = 1; counter <= 15; counter += 2) {
        numbers *= counter;
    }

    std::cout << "Product: " << numbers << std::endl;
    return 0;

}

