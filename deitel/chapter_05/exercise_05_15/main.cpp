#include "GradeBook.hpp"
#include <iostream>
#include <cstring>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input course name: ";
    }
    std::string courseName;
    std::getline(std::cin, courseName);
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input teacher name: ";
    }
    std::string teacherName;
    std::getline(std::cin, teacherName);

    GradeBook examResult (courseName, teacherName);
    examResult.printCourseName();
    return examResult.inputGrades();
}

