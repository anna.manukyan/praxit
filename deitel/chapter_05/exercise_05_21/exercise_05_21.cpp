#include <iostream>

int
main()
{
    std::cout << "Input worker code: ";
    int workerCode;
    std::cin >> workerCode;

    switch (workerCode) {
    case 1: {
        int managerSalary;
        std::cout << "Input manager salary: ";
        std::cin >> managerSalary;
        std::cout << "Weekly salary: " << managerSalary << std::endl;
        break;
    }
    case 2: {
        std::cout << "The number of hours worked: ";
        int hoursWorked;
        std::cin >> hoursWorked;
        std::cout << "Input salary: ";
        int hourPrice;
        std::cin >> hourPrice;
        std::cout << "Salary: " << hoursWorked * hourPrice << "\n";
        break;
    }
    case 3: {
        std::cout << "Weekly sales (in dollars): ";
        int numberOfSales; 
        std::cin >> numberOfSales;
        double worker = numberOfSales * (0.057) + 250;
        std::cout << "Salary: " << worker << "\n";
        break;
    }
    case 4:
        std::cout << "Sold per week: ";
        int numberOfProduct;
        std::cin >> numberOfProduct;
        std::cout << "Input price: ";
        int price;
        std::cin >> price;
        std::cout << "Salary: " << price * numberOfProduct << "\n";
        break;
    default:
        std::cerr << "Error 1: Working code is not correct." << std::endl;
        return 1;
    }
    return 0;    
}
