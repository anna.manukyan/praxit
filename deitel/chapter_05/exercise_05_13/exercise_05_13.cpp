#include <iostream>

int
main()
{
    for (int i = 0; i < 5; ++i) {
        std::cout << "Input Number (1-30):";
        int number;
        std::cin >> number;
        if (number < 1 || number > 30) {
            std::cerr << "Error 1: The input numbers must be in the (1-30). "<< std::endl;
            return 1; 
        }
        for (int i1 = 0; i1 < number; ++i1) {
            std::cout << "*";
        }    
    }
    std::cout << std::endl;
    return 0;
}
