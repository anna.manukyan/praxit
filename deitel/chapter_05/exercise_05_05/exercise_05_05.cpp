#include <iostream>

int
main()
{
    std::cout << " Input of numbers: ";
    int quantity;
    std::cin >> quantity;

    int sum = 0;

    for (int i = 0; i < quantity; ++i) {
        std::cout << "Input number: ";
        int number;
        std::cin >> number;
        sum += number;

    }
    std::cout << "Numbers sum: " << sum << std::endl;
    return 0;
}

