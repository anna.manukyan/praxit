#include <iostream>

int
main()
{
      /// a)
    for (int length = 1; length <= 10; ++length) {
        for (int height = 1; height <= length; ++height) {
            std::cout << "*";
        }
        std::cout << "\n";
    }
    std::cout << "\n";

    /// b)
    for (int length = 10; length >= 1; --length) {
        for (int height = 1; height <= length; ++height) {
            std::cout << "*";
        }
        std::cout << "\n";
    }
    std::cout << "\n";

    /// c)
    for (int length = 10; length >= 1; --length) {
        for (int space = 1; space <= 10 - length; ++space) {
            std::cout << " ";
        }
        for (int height = 1; height <= length; ++height) {
            std::cout << "*";
        }
        std::cout << "\n";
    }
    std::cout << "\n";

     /// d)
    for (int length = 1; length <= 10; ++length) {
        for (int space = 1; space <= 10 - length; ++space) {
            std::cout << " ";
        }
        for (int height = 1; height <= length; ++height) {
            std::cout << "*";
        }
        std::cout << "\n";
    }
    std::cout << "\n";

    std::cout << "a) " << "\t\t" << "b) " << "\t\t" << "c) " << "\t\t" << "d) \n" << std::endl;
    for (int length = 1; length <= 10; ++length) {
        ///  a)
        for (int height = 1; height <= length; ++height) {
            std::cout << "*";
        }
        for (int space = 1; space <= 10 - length; ++space) {
            std::cout << " ";
        }
        std::cout << "\t";

        ///  b)
        for (int height = 10; height >= length; --height) {
            std::cout << "*";
        }
        for (int space = 1; space < length; ++space) {
            std::cout << " ";
        }
        std::cout << "\t";

        ///  c)
        for (int space = 1; space < length; ++space) {
            std::cout << " ";
        }
        for (int height = 10; height >= length; --height) {
            std::cout << "*";
        }
        std::cout << "\t";

        ///  d)
        for (int space = 1; space <= 10 - length; ++space) {
            std::cout << " ";
        }
        for (int height = 1; height <= length; ++height) {
            std::cout << "*";
        }
        std::cout << "\n";
    }

    return 0;
}

