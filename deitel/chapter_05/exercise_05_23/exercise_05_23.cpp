#include <iostream>

int
main()
{
    int size = 9;
    int halfSize = size / 2;

    for (int counter1 = -halfSize; counter1 <= halfSize; ++counter1) {
        for (int counter2 = -halfSize; counter2 <= halfSize; ++counter2) {
            if (std::abs(counter1) + std::abs(counter2) > halfSize) {
                std::cout << " ";
            } else {
                std::cout << "*";
            }
        }

        std::cout << std::endl;
    }
    return 0;

}
