#include <iostream>
#include <limits>

int
main()
{
    std::cout << "Input quantity: ";
    int quantity;
    std::cin >> quantity;
    if (quantity < 0) {
        std::cerr << "Error 1: Quantity cannot be negative." << std::endl;
        return 1;    
    }

    int smallest = std::numeric_limits<int>::max();
    for (int i = 0; i < quantity; ++i) {
        std::cout << "Input number: ";
        int number;
        std::cin >> number;
        if (number <= smallest) {
            smallest = number;
        }
    }
    std::cout << "Smallest: " << smallest << std::endl;
    return 0;
}
