#include <iostream>

int
main()
{
   std::cout << "Input growth of rhombus: ";
    int rhombusLength;
    std::cin >> rhombusLength;
    if (rhombusLength < 0) {
        std::cout << "Error 3: The growth of rhombus cannot be negative." << std::endl;
        ::exit(3);
    
    }
    std::cout << "Input symbol of rhombus: ";
    char symbolOfRhombus;
    std::cin >> symbolOfRhombus;
    int center = rhombusLength / 2;
    for (int counter1 = -center; counter1 <= center; ++counter1) {
        for (int counter2 = -center; counter2 <= center; ++counter2) {
            if (std::abs(counter1) + std::abs(counter2) > center) {
                std::cout << " ";
            } else {
                std::cout << symbolOfRhombus;
            }
        }
        std::cout << std::endl;
    }
}
 

