#include <iostream>

int
main()
{
    int number1, number2, number3; 
    std::cout << "Input three different integers: "; 
    std::cin >> number1 >> number2 >> number3; 

    int largest = number1;
    if (number2 > largest) {
        largest = number2;
    }
                             
    if (number3 > largest) {
        largest = number3;
    }

    int smallest = number1;
    if (number2 < smallest) {
        smallest = number2;
    }      

    if (number3 < smallest) { 
        smallest = number3;
    }

    std::cout << "Sum is " << number1 + number2 + number3 << std::endl;
    std::cout << "Average is " << (number1 + number2 + number3) / 3 << std::endl;
    std::cout << "Product is " << number1 * number2 * number3 << std::endl;
    std::cout << "Smallest is " << smallest << std::endl;
    std::cout << "Largest is " << largest << std::endl;

    return 0;
}
