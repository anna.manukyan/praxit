#include <iostream>

int 
main()
{
    int number1, number2; // declaration

    std::cout << "Enter two integers: "; // prompt
    std::cin >> number1 >> number2; // input to numbers

    if (number1 > number2) {
        std::cout << number1 << " is larger " << number2 << std::endl;
        return 0;
    }

    if (number1 < number2) {
        std::cout << number1 << " is smaller " << number2 << std::endl;
        return 0;
    }

    std::cout << "These numbers are equal" << std::endl;

    return 0;
}

