#include <iostream>

int 
main()
{
    int number;
    std::cout << "Enter a five-digit number: ";
    std::cin >> number;
    
    if (number < 10000) {
        std::cout << "Error 1: Not a five-digit number." << std::endl;
        return 1;
    }

    if (number > 99999) {
        std::cout << "Error 1: Not a five-digit namber." << std::endl;
        return 1;
    }
    
    std::cout << number / 10000 << " ";
    number = number % 10000;
    std::cout << number / 1000 << " ";
    number = number % 1000;
    std::cout << number / 100 << " ";
    number = number % 100;
    std::cout << number / 10 << " ";
    number = number % 10;
    std::cout << number << std::endl;
    return 0;
}

