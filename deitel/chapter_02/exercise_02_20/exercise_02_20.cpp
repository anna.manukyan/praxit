#include <iostream>

int 
main()
{
    int radius; /// declaration

    std::cout << "Enter the circle radius: "; /// prompt
    std::cin >> radius;

    if (radius < 0) {
        std::cout << "Error 1: The second number cannot be negative." << std::endl;
        return 1;
    }
    

    std::cout << "Diameter is " << radius * 2.0 << std::endl;
    std::cout << "Circumference is " << 2 * 3.14159 * radius << std::endl;
    std::cout << "Area is " << 3.14159 * radius * radius << std::endl;
    
    return 0;
 }

