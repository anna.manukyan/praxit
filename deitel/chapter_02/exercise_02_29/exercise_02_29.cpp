#include <iostream>

int 
main()

{
    int number = 0;
    std::cout << "number\tsquare\tcube\n"
              << number << '\t' << number * number << '\t' << number * number * number << "\n";

    number = 1;
    std::cout << number << '\t' << number * number << '\t' << number * number * number << "\n";

    number = 2;
    std::cout << number << '\t' << number * number << '\t' << number * number * number << "\n";

    number = 3;
    std::cout << number << '\t' << number * number << '\t' << number * number * number << "\n";

    number = 4;
    std::cout << number << '\t' << number * number << '\t' << number * number * number << "\n";

    number = 5;
    std::cout << number << '\t' << number * number << '\t' << number * number * number << "\n";

    number = 6;
    std::cout << number << '\t' << number * number << '\t' << number * number * number << "\n";

    number = 7;
    std::cout << number << '\t' << number * number << '\t' << number * number * number << "\n";

    number = 8;
    std::cout << number << '\t' << number * number << '\t' << number * number * number << "\n";

    number = 9;
    std::cout << number << '\t' << number * number << '\t' << number * number * number << "\n";

    number = 10;
    std::cout << number << '\t' << number * number << '\t' << number * number * number << std::endl;
    return 0;
}

