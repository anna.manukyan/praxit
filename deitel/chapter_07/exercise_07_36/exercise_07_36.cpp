#include <iostream>

void printArray(const int array[], const int start, const int end);
void getArrayElements(int array[], const int size);

int
main()
{
    const int ARRAY_SIZE = 10;
    int array[ARRAY_SIZE] = {};
    getArrayElements(array, ARRAY_SIZE);
    printArray(array, 0, ARRAY_SIZE);

    return 0;
}

inline void
getArrayElements(int array[], const int size)
{
    
    std::cout << "Input " << size << " integers: ";
    for (int i = 0; i < size; ++i) {
        std::cin >> array[i];
    }
}

inline void
printArray(const int array[], const int start, const int end)
{
    if (end == start) {
        return;
    }
    std::cout << array[end - 1] << std::endl;
    printArray(array, start, end - 1);
}
