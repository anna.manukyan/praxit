#include <iostream>
#include "Date.hpp"

int
main()
{
    std::cout << "Enter the day, month, year: ";
    int day;
    int month;
    int year;
    std::cin >> day >> month >> year;
    Date showDate(day, month, year);
    showDate.displayDate(); 
    return 0;
}

