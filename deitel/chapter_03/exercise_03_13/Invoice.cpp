#include <iostream>
#include "Invoice.hpp"

Invoice::Invoice(std::string partNumber, std::string partDescription, int itemQuantity, int itemPrice)
{
    setPartNumber(partNumber);
    setPartDescription(partDescription);
    setItemQuantity(itemQuantity);
    setItemPrice(itemPrice);
}

void
Invoice::setPartNumber(std::string partNumber)
{
    partNumber_ = partNumber;
}

std::string
Invoice::getPartNumber()
{
    return partNumber_;
}

void
Invoice::setPartDescription(std::string partDescription)
{
    partDescription_ = partDescription;
}

std::string
Invoice::getPartDescription()
{
    return partDescription_;
}

void
Invoice::setItemQuantity(int itemQuantity)
{
    if (itemQuantity < 0) {
        std::cout << "Info 1: Given quantity is negative. Resetting to 0..." << std::cout;
        itemQuantity = 0;
    }
    itemQuantity_ = itemQuantity;
}

int
Invoice::getItemQuantity()
{
    return itemQuantity_;
}

void
Invoice::setItemPrice(int itemPrice)
{
    if (itemPrice < 0) {
        std::cout << "Info 2: Given price is negative. Resetting to 0..." << std::cout;
        itemPrice = 0;
    }
    itemPrice_ = itemPrice;
}

int
Invoice::getItemPrice()
{
    return itemPrice_;
}

int
Invoice::getInvoiceAmount()
{
    return getItemQuantity() * getItemPrice();
}
