#include <iostream>
#include "Account.hpp"

Account::Account(int balance)
{
    if (balance < 0) {
        std::cout << "Info 1: Given balance is negative. Resetting to 0..." << std::endl;
        accountBalance_ = 0;
    }
    accountBalance_ = balance;
}

void
Account::credit(int credit)
{
    accountBalance_ = accountBalance_ + credit;
}

void
Account::debit(int debit)
{
    if (accountBalance_ < debit) {
        std::cout << "Info 2: Debit amount exeeded account balance." << std::endl;
    }
    accountBalance_ = accountBalance_ - debit;
}

int
Account::getAccountBalance()
{
    return accountBalance_;
}

