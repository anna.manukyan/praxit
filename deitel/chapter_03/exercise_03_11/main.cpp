#include <iostream>
#include "GradeBook.hpp"

int
main()
{
    GradeBook gradeBook1("Math for beginners", "Anna Manukyan");
    GradeBook gradeBook2("Algebra", "Anna Manukyan");

    std::cout << "GradeBook1 created for: " << gradeBook1.getCourseName() << std::endl;
    std::cout << "This course is presented by: " << gradeBook1.getTeacherName() << std::endl;
    std::cout << "GradeBook1 created for: " << gradeBook2.getCourseName() << std::endl;
    std::cout << "This course is presented by: " << gradeBook2.getTeacherName() << std::endl;

    gradeBook1.displayMessage();

    return 0;

}
