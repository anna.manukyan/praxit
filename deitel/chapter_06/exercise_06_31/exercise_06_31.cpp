#include <iostream>
#include <cmath>
 
int reversedNumber(int number);

int 
main()
{
   std::cout << "Enter integer: ";
   int number;
   std::cin >> number;
   std::cout << "Reversed digits: " << reversedNumber(number) << std::endl;
   return 0;
}

int
reversedNumber(int number)
{
    int reversedNumber = 0;
    while (number != 0) {
        const int remainder = number % 10;
        reversedNumber = reversedNumber * 10 + remainder;
        number /= 10;
    }
    return reversedNumber;
}

