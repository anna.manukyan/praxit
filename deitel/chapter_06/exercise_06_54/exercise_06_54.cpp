#include <iostream>
#include <cstdlib>
#include <cassert>

int rollDice();
bool game();
void printBankBalance(const unsigned int bankBalance);
void chatter(); 

int
main ()
{
    ::srand(::time(0));
    int bankBalance = 1000;
    while (bankBalance > 0) {
        printBankBalance(bankBalance);
        std::cout << "Enter your bet: ";
        int wager = 0;
        while (true) {
            std::cin >> wager;
            if (wager <= bankBalance) {
                break;
            }
            std::cout << "There are not enough funds in your account, try again." << std::endl;  
        }
        const bool gameResult = game();
        if (gameResult) {
            bankBalance += wager;
        } else {
            bankBalance -= wager;
        }

        if (0 == bankBalance) {
            std::cout << "Sorry but you went bankrupt.\a" << std::endl;
            printBankBalance(bankBalance);
        }
    }
    return 0;
}

void
printBankBalance(const unsigned int bankBalance)
{
    std::cout << "Your current bank balance: " << bankBalance << "$" << std::endl;
}

int
dices()
{
    const int dice1 = 1 + ::rand() % 6;
    const int dice2 = 1 + ::rand() % 6;
    const int sum = dice1 + dice2;
    std::cout << "Player rolled " << dice1 << " + " << dice2 << " = " << sum << std::endl;
    return sum;
}

bool
game()
{
    enum Status {CONTINUE, WON, LOST};
    Status gameStatus;
    int myPoint = 0;
    const int sumOfDice = dices();
    switch (sumOfDice) {
    case 7:
    case 11: gameStatus = WON;  break;
    case 2:
    case 3:
    case 12: gameStatus = LOST; break;
    default:
        myPoint = sumOfDice;
        gameStatus = CONTINUE;
        std::cout << "Point is " << myPoint << std::endl;
        break;
    }
    chatter();
    while (CONTINUE == gameStatus) {
        const int sumOfDice = dices();
        if (sumOfDice == myPoint) {
            gameStatus = WON;
        } else if (7 == sumOfDice) {
            gameStatus = LOST;
        }
        if (WON == gameStatus) {
            std::cout << "Player wins" << std::endl;
            return true;
        }

        std::cout << "Player loses" << std::endl;
        return false;
    }
    return false;
}

void
chatter()
{
    const int dialogue = rand() % 6;
    switch (dialogue) {
    case 0: std::cout << "Oh, you're going for broke, huh?" << std::endl;                     break;
    case 1: std::cout << "Aw cmon, take a chance!" << std::endl;                              break;
    case 2: std::cout << "You're up big. Now's the time to cash in your chips!" << std::endl; break;
    case 3: std::cout << "Your money smells mine! Sniff sniff" << std::endl;                  break;
    case 4: std::cout << "Don't give up, head up!" << std::endl;                              break;
    case 5: std::cout << "Keep rolling!" << std::endl;                                        break;
    default: assert(0);                                                                       break;
    }
}



