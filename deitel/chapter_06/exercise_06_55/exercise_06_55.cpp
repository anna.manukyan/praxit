#include <iostream>
#include <cassert>

inline double circleArea(const double radius);

int
main()
{
    std::cout << "Input radius: ";
    double radius;
    std::cin >> radius;
    if (radius < 0) {
        std::cerr << "Error 1: Rasius cannot be negative." << std::endl;
        return 1;
    }
    std::cout << "The area of circle is " << circleArea(radius) << std::endl;
    return 0;
}

inline double
circleArea(double const radius)
{
    assert(radius >= 0);
    const double pi = 3.14;
    return pi * radius * radius;
}
