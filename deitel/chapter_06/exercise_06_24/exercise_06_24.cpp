#include <iostream>
#include <cassert>

void drawFigure(const int numberOfFigures);
void drawTriangle();
void drawSquare();
void drawRombus();

int
main()
{
    std::cout << "Print 1 for triangle, 2 for square, 3 for rombus: ";
    int numberOfFigures;
    std::cin >> numberOfFigures;
    if (numberOfFigures < 1 || numberOfFigures > 3) {
        std::cerr << "Error 1: Invalid number.";
        return 1;
    }
    drawFigure(numberOfFigures);
    return 0;
}

void
drawFigure(const int numberOfFigures)
{
    switch (numberOfFigures) {
    case 1: drawTriangle(); break;
    case 2: drawSquare(); break;
    case 3: drawRombus(); break;
    default:
        std::cout << "Assert 1: Invalid number." << std::endl;
        assert(0);
    }
}

void
drawTriangle()
{
    std::cout << "Input side of triangle: ";
    int sideOfTriangle;
    std::cin >> sideOfTriangle;
    if (sideOfTriangle < 0) {
        std::cout << "Error 2: The side of triangle cannot be negative." << std::endl;
        ::exit(2);
        
    }
    std::cout << "Input symbol of triangle: ";
    char symbolOfTriangle;
    std::cin >> symbolOfTriangle;
    for (int counter1 = 0; counter1 < sideOfTriangle; ++counter1) {
        for (int counter2 = 0; counter2 <= counter1; ++counter2) {
            std::cout << symbolOfTriangle;
        }
        std::cout << std::endl;
    }
}

void
drawRombus()
{
    std::cout << "Input growth of rhombus: ";
    int rhombusLength;
    std::cin >> rhombusLength;
    if (rhombusLength < 0) {
        std::cout << "Error 3: The growth of rhombus cannot be negative." << std::endl;
        ::exit(3);
    
    }
    std::cout << "Input symbol of rhombus: ";
    char symbolOfRhombus;
    std::cin >> symbolOfRhombus;
    int center = rhombusLength / 2;
    for (int counter1 = -center; counter1 <= center; ++counter1) {
        for (int counter2 = -center; counter2 <= center; ++counter2) {
            if (std::abs(counter1) + std::abs(counter2) > center) {
                std::cout << " ";
            } else {
                std::cout << symbolOfRhombus;
            }
        }
        std::cout << std::endl;
    }
}

void 
drawSquare()
{
    std::cout << "Input side length: ";
    int sideLength;
    std::cin >> sideLength;
    if (sideLength <= 0) {
        std::cerr << "Error 4: Side lengt cannot be negative or zero." << std::endl;
        ::exit(4);
    
    }
    std::cout << "Input simbol: ";
    char fillCharacter;
    std::cin >> fillCharacter;
     for (int i = 0; i < sideLength; ++i) {
        for (int j = 0; j < sideLength; ++j) {
            std::cout << fillCharacter;
        }
        std::cout << std::endl;
    }
}

