#include <iostream>

int 
main()
{
    std::cout << "Enter in randam range 1-2: \n";
    int n = 1 + ::rand() % 2;
    std::cout << n << std::endl;
    
    std::cout << "Enter in randam range 1 - 100: \n";
    n = 1 + ::rand() % 100;
    std::cout << n << std::endl;
    
    std::cout << "Enter in randam range 0 - 9: \n";
    n = ::rand() % 10;
    std::cout << n << std::endl;

    std::cout << "Enter in randam range 1000 - 1112: \n";
    n = 1000 + ::rand() % 113;
    std::cout << n << std::endl;

    std::cout << "Enter in randam range -1 - 1: \n";
    n = -1 + ::rand() % 3;
    std::cout << n << std::endl;
    
    std::cout << "Enter in randam range -3 - 11: \n";
    n = -3 + ::rand() % 15;
    std::cout << n << std::endl;
    return 0;
}
