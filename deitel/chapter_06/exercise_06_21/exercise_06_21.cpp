#include <iostream>
#include <unistd.h>

bool evenNumber(const int number);

int
main()
{
    std::cout << "Input number: ";
    int number;
    std::cin >> number;
    std::cout << "Number " << number << " is even = "
              << std::boolalpha << evenNumber(number) << std::endl;

    return 0;
}

inline bool
evenNumber(const int number)
{
    return 0 == (number & 1);
}


