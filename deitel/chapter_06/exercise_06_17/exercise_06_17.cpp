#include <iostream>

int
main()
{
    std::cout << " Random numbers range 2, 4, 6, 8, 10 \n"
              << 2 + std::rand() % 5 * 2 << std::endl;
    std::cout << " Random numbers range 3, 5, 7, 9, 11 \n"
              << 3 + std::rand() % 5 * 2 << std::endl;
    std::cout << " Random numbers range 6, 10, 14, 18, 22 \n"
              << 6 + std::rand() % 5 * 4 << std::endl;
    return 0;
}
