 What is the purpose of the unary scope resolution operator?

 Ans: The unary operation of resolving the scope allows you to access a global variable (scope - the whole program) 
 from a block in which a local variable can be declared (scope begins with its declaration and ends with the closing of the current block) with the same name.
