#include <iostream>

bool perfect(const int number);
void printPerfectNumber(const int number);

int
main()
{
    for (int number = 1; number <= 1000; ++number) {
        const bool perfectNumber = perfect(number);
        if (perfectNumber) {
            printPerfectNumber(number);
        }
    }
    return 0;
}

void
printPerfectNumber(const int number)
{
    std::cout << "Perfect number: " << number << " = ";
    for (int counter = 1; counter <= number / 2; ++counter) {
        if (0 == number % counter) {
            std::cout << counter;
            if (counter < number / 2) {   
                std::cout << " + ";
            } 
         }
    }
    std::cout << std::endl;
}

bool
perfect(const int number)
{
    int perfectNumber = 0;
    for (int counter = 1; counter < number; ++counter) {
        if (0 == number % counter) {
            perfectNumber += counter;
        }
    }

    return number == perfectNumber;
}

            
