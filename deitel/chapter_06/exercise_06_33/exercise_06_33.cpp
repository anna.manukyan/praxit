#include <iostream>
#include <unistd.h>
#include <cassert>

int qualityPoints(const int average);

int
main()
{
    std::cout << "Input student average grade: ";
    int average;
    std::cin >> average;
    if (average < 0 || average > 100) {
        std::cerr << "Error 1: Average grade cannot be 0 or > 100. " << std::endl;
        return 1;
    }
    std::cout << "Student grade: " << qualityPoints(average) << std::endl;
    return 0; 
}

int
qualityPoints(int average)
{
   assert(average >= 0 && average <= 100);
   if (100 == average) {
       --average;
    }
   return average / 10 - 5;
}
