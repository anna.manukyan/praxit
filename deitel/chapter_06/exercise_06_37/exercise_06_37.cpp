#include <iostream>
#include <cassert>

void multiplicationTable();
void correctAnswer(); 
void incorrectAnswer();

int
main()
{   
    ::srand(::time(0));
    multiplicationTable();
    std::cout << "Exiting..." << std::endl;
    return 0;
}

void
multiplicationTable()
{
    int answerCounter = 0;
    int rightAnswer = 0;
    int wrongAnswer = 0; 
    while (true) {
        const int number1 = ::rand() % 10;
        const int number2 = ::rand() % 10;
        while (true) {
            std::cout << "How much will be: " << number1 << "*" << number2;
            std::cout << "\nAnswer: ";
            int answer;
            std::cin >> answer;
            ++answerCounter;
            if(number1 * number2 == answer) {
                ++rightAnswer;
                correctAnswer();
                break;
            }
            ++wrongAnswer;
            incorrectAnswer(); 
        }

        std::cout << "Do you want to contineue? (0 - Yes; 1 - No): ";
        bool number;
        std::cin >> number;
        if (number) {
            break;
        }
    }

    if(answerCounter * 0.75 > static_cast<double>(rightAnswer)) {
        std::cout << "Please ask your teacher to help you. " << std::endl;
    }
}

void
correctAnswer()
{ 
    const int randamAnsver = 1 + ::rand() % 4;
    switch (randamAnsver) {
    case 1: std::cout << " Very good! " << std::endl; break;
    case 2: std::cout << " Excellent! " << std::endl; break;
    case 3: std::cout << " Nice work! " << std::endl; break;
    case 4: std::cout << " Keep up the good work! " << std::endl; break;
    default: assert(0); break;
    }
}

void
incorrectAnswer()
{
    const int randamAnsver = 1 + ::rand() % 4;
    switch (randamAnsver) {
    case 1: std::cout << " No. Please try again. " << std::endl; break;
    case 2: std::cout << " Wrong. Try once more. " << std::endl; break;
    case 3: std::cout << " Don't give up! " << std::endl; break;
    case 4: std::cout << " No. Keep trying." << std::endl; break;
    default: assert(0); break;
    }
}    


