#include <iostream>

int tripleByValue(int count);
int tripleByReferance(int& count);

int
main()
{
    std::cout << "Input a number: ";
    int count;
    std::cin >> count;
    std::cout << "The result of tripleByValue function " << tripleByValue(count) << std::endl;
    std::cout << "The value of count after tripleByValue function call " << count << std::endl;
    std::cout << "The result of tripleByReference function " << tripleByReferance(count) << std::endl;
    std::cout << "The value of count after tripleByReference function call " << count << std::endl;
    return 0;
}

int
tripleByValue(int count)
{
    return count *= 3;
}

int
tripleByReferance(int& count)
{
    return count *= 3;
}
