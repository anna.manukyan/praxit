#include <iostream>
#include <cassert>

bool isMultiple(const int number1, const int number2);

int
main()
{
    std::cout << "Input number 1: ";
    int number1;
    std::cin >> number1;

    std::cout << "Input number 2: ";
    int number2;
    std::cin >> number2;
    if (0 == number2) {
        std::cerr << "Error 1: Number 1 cannot be 0." << std::endl;
        return 1;
    } 
    std::cout << number2 << "is multiple of " << number1 << " = " << std::boolalpha 
              << isMultiple(number1, number2) << std::endl;
    return 0;
}

inline bool
isMultiple(const int number1, const int number2)
{
    assert(number2 != 0);
    return 0 == number1 % number2;
}

