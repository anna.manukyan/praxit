#include <iostream>
#include <cassert>
#include <iomanip>
#include <cmath>


void mathematicalCalculations();
void correctAnswer(); 
void incorrectAnswer();
int randomNumber(const int number);
int getOperationResult(const int operationNumber, const int number1, const int number2);
char getOperation(const int operationNumber);

int
main()
{   
    ::srand(::time(0));
    mathematicalCalculations();
    std::cout << "Exiting..." << std::endl;
    return 0;
}

void
mathematicalCalculations()
{
    int answerCounter = 0;
    int rightAnswer = 0;
    int wrongAnswer = 0; 
    while (true) {
        std::cout << "Enter complexity level: ";
        int level;
        std::cin >> level;
        if (level > 3 || level < 1) {
            std::cerr << "Error 1: Invalid complexity level." << std::endl;
            ::exit(1);
        }
        
        std::cout << "Select aritmetic operation: (1 - (+), 2 - (-), 3 - (*), 4 - (/), 5 - random): ";
        int operationNumber;
        std::cin >> operationNumber;
        if (operationNumber < 1 || operationNumber > 5) {
            std::cerr << "Error 2: Invalid operation number." << std::endl;
            ::exit(2);
        }

        if (5 == operationNumber) {
            operationNumber = 1 +::rand() % 4;
        }

        const char operation = getOperation(operationNumber);
        const int number1 = randomNumber(level);
        const int number2 = randomNumber(level);
        const int trueAnswer = getOperationResult(operationNumber, number1, number2);
        while (true) {
            std::cout << "How much will be: " << number1 << " " << operation << " " << number2;
            std::cout << "\nAnswer: ";
            int answer;
            std::cin >> answer;
            ++answerCounter;
            if (trueAnswer == answer) {
                ++rightAnswer;
                correctAnswer();
                break;
            }
            ++wrongAnswer;
            incorrectAnswer(); 
        }

        std::cout << "Do you want to contineue? (0 - Yes; 1 - No): ";
        bool choice;
        std::cin >> choice;
        if (choice) {
            break;
        }
    }

    if (answerCounter * 0.75 > static_cast<double>(rightAnswer)) {
        std::cout << "Please ask your teacher to help you. " << std::endl;
    }
}

void
correctAnswer()
{ 
    const int randamAnsver = 1 + ::rand() % 4;
    switch (randamAnsver) {
    case 1: std::cout << " Very good! " << std::endl; break;
    case 2: std::cout << " Excellent! " << std::endl; break;
    case 3: std::cout << " Nice work! " << std::endl; break;
    case 4: std::cout << " Keep up the good work! " << std::endl; break;
    default: assert(0); break;
    }
}

void
incorrectAnswer()
{
    const int randamAnsver = 1 + ::rand() % 4;
    switch (randamAnsver) {
    case 1: std::cout << " No. Please try again. " << std::endl; break;
    case 2: std::cout << " Wrong. Try once more. " << std::endl; break;
    case 3: std::cout << " Don't give up! " << std::endl; break;
    case 4: std::cout << " No. Keep trying." << std::endl; break;
    default: assert(0); break;
    }
}    

int
randomNumber(const int number)
{
    assert(number <= 3 && number >= 1);
    const int range = ::pow(10, number);
    const int number3 = ::rand() % range;
    return number3;

}

int
getOperationResult(const int operationNumber, const int number1, const int number2)
{
    switch (operationNumber) {
    case 1: return number1 + number2;
    case 2: return number1 - number2;
    case 3: return number1 * number2;
    case 4: return number1 / number2;
    }
    assert(0);
    return 0;
}

char 
getOperation(const int operationNumber)
{
    switch (operationNumber) {
    case 1: return '+';
    case 2: return '-';
    case 3: return '*';
    case 4: return '/';
    }
    assert(0);
    return '+';
}

