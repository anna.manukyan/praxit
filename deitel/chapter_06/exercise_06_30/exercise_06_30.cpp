#include <iostream>
#include <cmath>

bool primeNumber(const int number);

int
main()
{   
    std::cout << "The list of prime numbers from range 1 - 10000. " << std::endl;
    for (int number = 2; number <= 10000; ++number) {
        if (primeNumber(number)) {
            std::cout << "Prime number: " << number << std::endl;
        }
    }
    return 0;
}

 /// In mathematics each number has it's square root as integer
 /// or floating point number. For this task integer type square
 /// roots are must, so the largest divisor every number
 ///  have is it's square root.
 ///spending more than the square root of a number is pointless.
 ///This is the most optimal.

bool
primeNumber(const int number)
{
    for (int counter = 2; counter <= std::sqrt(number); ++counter) {
        if (0 == number % counter) {
            return false;
        }
    }

    return true;
}

