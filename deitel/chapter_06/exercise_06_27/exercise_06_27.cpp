#include <iostream>
#include <iomanip>

double celsius(const int temperature);
double fahrenheit(const int temperature); 

int
main()
{
    for (int temperature = 1; temperature <= 100; ++temperature) {
        std::cout << std::setw(8) << temperature << " celsius = " << std::setw(3) << fahrenheit(temperature) << " fahrenheit" << std::endl;
    }
    std::cout << std::endl;

    for (int temperature = 32; temperature <= 212; ++temperature) {
        std::cout << std::setw(8) << temperature << " fahrenheit = " << std::setw(3) << celsius(temperature) << " celsius" << std::endl;
    }
    std::cout << std::endl;
    return 0;
}

double 
celsius(const int temperature)
{
    const double fahrenheit = static_cast<double>(temperature) - 32 * 5/9; 
    return fahrenheit;

}

double 
fahrenheit(const int temperature)
{
    const double celsius = static_cast<double>(temperature) * 9/5 + 32;
    return celsius;

}

