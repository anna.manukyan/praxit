#include <iostream>
#include <cmath>

int
main()
{
    std::cout << "Math function" << std::endl;
    std::cout << "Input number 1: ";
    double number1;
    std::cin >> number1;
    std::cout << "Rounding to the smallest integer: " << std::ceil(number1) << std::endl;
    std::cout << "Trigonometric cos: " << std::cos(number1) << std::endl;
    std::cout << "Trigonometric sin: " << std::sin(number1) << std::endl;
    std::cout << "Trigonometric tan: " << std::tan(number1) << std::endl;
    std::cout << "Exponential function: " << std::exp(number1) << std::endl;
    std::cout << "Absolute value: " << std::fabs(number1) << std::endl;
    std::cout << "Squar rute: " << std::sqrt(number1) << std::endl;
    std::cout << "Rounding to the largest integer: " << std::floor(number1) << std::endl;
    std::cout << "Input number 2: ";
    double number2;
    std::cin >> number2;
    std::cout << "Remainder of " << number1 << "/" << number2 << " as number float point: " << std::fmod(number1, number2) << std::endl;
    std::cout << "Natural logaritm: " << std::log(number1) << std::endl;
    std::cout << "Decimal logaritm: " << std::log10(number1) << std::endl;
    std::cout << "The pow() function computes a base number raised to the power of exponent number: " << std::pow(number1, number2) << std::endl;
    return 0;
}
