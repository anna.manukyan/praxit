#include <iostream>

int
main()
{
    static int counter = 1;
    std::cout << counter << std::endl;
    ++counter;
    if (8 == counter) {
        return 0;
    }
    return main();
} 
