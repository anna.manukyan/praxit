#include "headers/Date.hpp"
#include <gtest/gtest.h>

TEST(Date, DefaultConstructor)
{
    const Date d1;
    EXPECT_EQ(d1.getYear(), 2000);
    EXPECT_EQ(d1.getMonth(), 1);
    EXPECT_EQ(d1.getDay(), 1);
    
}

TEST(Date, YearConstructor)
{
    const Date d1(2011);
    EXPECT_EQ(d1.getYear(), 2011);
    EXPECT_EQ(d1.getMonth(), 1);
    EXPECT_EQ(d1.getDay(), 1);
}

TEST(Date, MonthConstructor)
{
    const Date d1(2000, 5);
    EXPECT_EQ(d1.getYear(), 2000);
    EXPECT_EQ(d1.getMonth(), 5);
    EXPECT_EQ(d1.getDay(), 1);
}

TEST(Date, DayConstructor)
{
    const Date d1(2000, 3, 4);
    EXPECT_EQ(d1.getYear(), 2000);
    EXPECT_EQ(d1.getMonth(), 3);
    EXPECT_EQ(d1.getDay(), 4);
}

TEST(Date, NextYear)
{
    Date d1(2000, 3, 4);
    d1.nextYear();
    EXPECT_EQ(d1.getYear(), 2001);
    EXPECT_EQ(d1.getMonth(), 3);
    EXPECT_EQ(d1.getDay(), 4);
}

TEST(Date, NextMonth)
{
    Date d1(2000, 5, 1);
    d1.nextMonth();
    EXPECT_EQ(d1.getYear(), 2000);
    EXPECT_EQ(d1.getMonth(), 6);
    EXPECT_EQ(d1.getDay(), 1);
}

TEST(Date, NextMonthAndYear)
{
    Date d1(2000, 12, 1);
    d1.nextMonth();
    EXPECT_EQ(d1.getYear(), 2001);
    EXPECT_EQ(d1.getMonth(), 1);
    EXPECT_EQ(d1.getDay(), 1);
}

TEST(Date, NextDay)
{
    Date d1(2000, 5, 1);
    d1.nextDay();
    EXPECT_EQ(d1.getYear(), 2000);
    EXPECT_EQ(d1.getMonth(), 5);
    EXPECT_EQ(d1.getDay(), 2);
}

TEST(Date, NextDayAndMonth)
{
    Date d1(2000, 5, 31);
    d1.nextDay();
    EXPECT_EQ(d1.getYear(), 2000);
    EXPECT_EQ(d1.getMonth(), 6);
    EXPECT_EQ(d1.getDay(), 1);
}

TEST(Date, IsLeapYear)
{
    const Date d1(2021);
    EXPECT_FALSE(d1.isLeapYear());
}

TEST(Date, IsLeapYear400)
{
    const Date d1(2000);
    EXPECT_TRUE(d1.isLeapYear());
}

TEST(Date, IsLeapYear100)
{
    const Date d1(2100);
    EXPECT_FALSE(d1.isLeapYear());
}

TEST(Date, IsLeapYear4)
{
    const Date d1(2020);
    EXPECT_TRUE(d1.isLeapYear());
}

TEST(Date, Print)
{
    const Date d1(2021, 4, 23);
    std::stringstream s;
    d1.print(s);
    EXPECT_STREQ(s.str().c_str(), "2021-04-23");
}

int
main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

