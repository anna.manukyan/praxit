#ifndef __DATE_HPP__
#define __DATE_HPP__

#include <ostream>

class Date
{
public:
    Date(const int year = 2000, const int month = 1, const int day = 1);
    int getYear() const { return year_; }
    int getMonth() const { return month_; }
    int getDay() const { return day_; }
    void nextYear();
    void nextMonth();
    void nextDay();
    void print(std::ostream& out) const;
    bool isLeapYear() const;
private:
    int year_;
    int month_;
    int day_;
};
#endif /// __DATE_HPP__

