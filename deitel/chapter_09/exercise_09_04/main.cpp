#include "headers/Time.hpp"

#include <iostream>
 
int 
main()
{
    
    Time t1;
    Time t2(12);
    Time t3(12, 15);
    Time t4(12, 15, 24);

    t1.getTime();
    t2.getTime();
    t3.getTime();
    t4.getTime();

    return 0;
}
