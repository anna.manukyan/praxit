#ifndef __COMPLEX_HPP__
#define __COMPLEX_HPP__

class Complex
{
public:
    Complex(const double real, const double imaginary);
    double getReal() const { return real_; }
    double getImaginary() const { return imaginary_; }
    Complex plus(const Complex& rhv) const;
    Complex minus(const Complex& rhv) const;
    Complex multiply(const Complex& rhv) const;
    Complex divide(const Complex& rhv) const;
    void print() const;
private:
    double real_;
    double imaginary_;
};

#endif /// __COMPLEX_HPP__

