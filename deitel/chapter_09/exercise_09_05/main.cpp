#include <iostream>
#include <unistd.h>

#include "headers/Complex.hpp"

int
main()
{
    double real1, imaginary1;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input first pair of real and imaginary numbers: ";
    }
    std::cin >> real1 >> imaginary1;

    double real2, imaginary2;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input second pair of real and imaginary numbers: ";
    }
    std::cin >> real2 >> imaginary2;

    const Complex complex1(real1, imaginary1);
    const Complex complex2(real2, imaginary2);

    const Complex sum = complex1.plus(complex2);
    const Complex difference = complex1.minus(complex2);
    const Complex multiplication = complex1.multiply(complex2);
    const Complex division = complex1.divide(complex2);

    std::cout << "The sum is ";
    sum.print();
    std::cout << std::endl;

    std::cout << "The difference is ";
    difference.print();
    std::cout << std::endl;

    std::cout << "The multiplication is ";
    multiplication.print();
    std::cout << std::endl;

    std::cout << "The division is ";
    division.print();
    std::cout << std::endl;

    return 0;
}

