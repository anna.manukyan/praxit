#include "headers/DateAndTime.hpp"
#include <iostream>

int
main()
{
    DateAndTime dateAndTime(2000, 1, 1, 23, 59, 56);

    for (int i = 0; i < 10; ++i) {
        dateAndTime.nextTime();
        dateAndTime.printTime();
        std::cout << "\n";
    }
    return 0;
}
