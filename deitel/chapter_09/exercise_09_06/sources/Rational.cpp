#include "headers/Rational.hpp"
#include <iostream>
#include <cmath>
#include <cassert>

Rational::Rational(int numerator, int denominator)
{
    numerator_ = numerator;
    denominator_ = denominator;
}

Rational
Rational::plus(const Rational& rhv) const
{
    const int numerator = (rhv.denominator_ * numerator_) + (denominator_ * rhv.numerator_);
    const Rational total(numerator, denominator_ * rhv.denominator_);
    return total;
}

Rational
Rational::minus(const Rational& rhv) const
{
    const int numerator = (rhv.denominator_ * numerator_) - (denominator_ * rhv.numerator_);
    const Rational total(numerator, denominator_ * rhv.denominator_);
    return total;
}

Rational
Rational::multiplication(const Rational& rhv) const
{
    const int numerator = numerator_ * rhv.numerator_;
    const int denominator = denominator_ * rhv.denominator_;
    const Rational total(numerator, denominator);
    return total;
}

Rational
Rational::division(const Rational& rhv) const
{
    const int numerator = numerator_ * rhv.denominator_;
    const int denominator = denominator_ * rhv.numerator_;
    const Rational total(numerator, denominator);
    return total;
}

void
Rational::printRational() const
{
    std::cout << numerator_ << "/" << denominator_;
}

void
Rational::normalize()
{
    if (numerator_ < 0 && denominator_ < 0) {
        numerator_ = ::abs(numerator_);
        denominator_ = ::abs(denominator_);
    }

    if (denominator_ < 0) {
        numerator_ = - numerator_;
        denominator_ = -denominator_;
    }

    const int divisor = gcd(::abs(numerator_), denominator_);
    denominator_ /= divisor;
    numerator_ /= divisor;
}

void
Rational::printFloatRational() const
{
    std::cout << static_cast<float>(numerator_) / denominator_ << std::endl;
}

int
Rational::gcd(int number1, int number2) const
{
    assert (number1 > 0);
    while (number1 != 0 && number2 != 0) {
        if (number1 > number2) {
            number1 %= number2;
        } else {
            number2 %= number1;
        }
    }
    return number1 + number2;
}

