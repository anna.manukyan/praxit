#include "headers/Rational.hpp"

#include <gtest/gtest.h>

TEST(Rational, plus)
{
    const Rational c1(4, 7), c2(1, 2);
    const Rational c3 = c1.plus(c2);
    EXPECT_EQ(c3.getNumerator(), 15);
    EXPECT_EQ(c3.getDenominator(), 14);
}

TEST(Rational, minus)
{
    const Rational c1(4, 7), c2(1, 2);
    const Rational c3 = c1.minus(c2);
    EXPECT_EQ(c3.getNumerator(), 1);
    EXPECT_EQ(c3.getDenominator(), 14);
}

TEST(Rational,  multiplication)
{
    const Rational c1(4, 7), c2(1, 2);
    Rational c3 = c1.multiplication(c2);
    EXPECT_EQ(c3.getNumerator(), 4);
    EXPECT_EQ(c3.getDenominator(), 14);
    c3.normalize();
    EXPECT_EQ(c3.getNumerator(), 2);
    EXPECT_EQ(c3.getDenominator(), 7);
}

TEST(Rational, division)
{
    const Rational c1(4, 7), c2(1, 2);
    const Rational c3 = c1.division(c2);
    EXPECT_EQ(c3.getNumerator(), 8);
    EXPECT_EQ(c3.getDenominator(), 7);
}

int
main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

