#include "headers/Time.hpp"

#include <iostream>
 
int 
main()
{
    Time time1(12, 15, 24);
    std::cout << "Time: ";
    time1.print();
    std::cout << "Incrementation.\n";

    for (int i = 0; i < 10; ++i) {
        time1.tickTime();
        time1.print();
    }
    return 0;
}

