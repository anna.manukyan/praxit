#include <iostream>

int
main()
{
    std::cout << "Input three numbers: ";
    double side1;
    std::cin >> side1;
    if (side1 <= 0) {
        std::cerr << "Error 1: Sides cannot be negative. " << std::endl;
        return 1;
    }
    double side2;
    std::cin >> side2;
    if (side2 <= 0) {
        std::cerr << "Error 1: Sides cannot be negative. " << std::endl;
        return 1;
    }
    double side3;
    std::cin >> side3;
    if (side3 <= 0) {
        std::cerr << "Error 1: Sides cannot be negative. " << std::endl;
        return 1;
    }

    int side1Square = side1 * side1;
    int side2Square = side2 * side2;
    int side3Square = side3 * side3;

    if (side1Square + side2Square == side3Square) {
        std::cout << "Theas numbers can make up the side of a right triangle. " << std::endl;
        return 0;
    }

    if (side2Square + side3Square == side1Square) {
        std::cout << "Theas numbers can make up the side of a right triangle. " << std::endl;
        return 0;
    }

    if (side1Square + side3Square == side2Square) {
        std::cout << "Theas numbers can make up the side of a right triangle. " << std::endl;
        return 0;
    }

    std::cout << "These numbers cannot make up the sides of a right triangle" << std::endl;
    return 0;
}

