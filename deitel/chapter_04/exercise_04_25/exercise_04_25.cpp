#include <iostream>

int
main()
{
    int square;
    std::cout << "Entre a number from 1 - 20: ";
    std::cin >> square;

    if (square <= 0) {
        std::cerr << "Error 1: The numbers cannot be negative or 0." << std::endl;
        return 1;
    }
    
    if (square > 20) {
        std::cerr << "Error 2: The number cannot be more than twenty." << std::endl;
        return 2;
    }       

    int row = 1;
    while (row <= square) {
        int counter = 1;
        while (counter <= square) {
            if (1 == row) {
                std::cout << "*";
            } else if (row == square) {
                std::cout << "*";
            } else if (1 == counter) {
                std::cout << "*";
            } else if (counter == square) {
                std::cout << "*";
            } else {
                std::cout << " ";
            }       
            ++counter;
        }   
        ++row;
        std::cout << std::endl;
    }
    return 0;
}

