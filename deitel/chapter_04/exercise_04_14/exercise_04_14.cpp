#include <iostream>
#include <iomanip>

int
main()
{
    while(true) {
        std::cout << "Enter account number (-1 to end): " << std::endl;
        int accountNumber = 0;
        std::cin >> accountNumber;
        if (-1 == accountNumber) {
            break;
        }

        if (accountNumber <= 0) {
            std::cout << "Error 1: Input positive number. " << std::endl;
            return 1;
        }
        std::cout << "Enter beginning balance: " << std::endl;
        double beginningBalance;
        std::cin >> beginningBalance;

        std::cout << "Enter total charges: " << std::endl;
        double totalCharges = 0;
        std::cin >> totalCharges;
        if (totalCharges < 0) {
            std::cout << "Error 2: Enter a number greater than zero. " << std::endl;
            return 2;
        }
        std::cout << "Enter total credits: " << std::endl;
        double totalCredits = 0;
        std::cin >> totalCredits;
        if (totalCredits < 0) {
            std::cout << "Error 2: Enter a number greater than zero. " << std::endl;
            return 2;
        }
        std::cout << "Enter credit limit: "<< std::endl;
        double creditLimit = 0;
        std::cin >> creditLimit;
        if (creditLimit < 0) {
            std::cout << "Error 2: Enter a number greater than zero. " << std::endl;
            return 2;
        }
        double newBalance = beginningBalance + totalCharges - totalCredits;
        std::cout << "New balance is " << std::setprecision (2) << std::fixed << newBalance << std::endl;
        if (newBalance > creditLimit) {
            std::cout << "Account: " << std::setprecision (2) << std::fixed << accountNumber << std::endl;
            std::cout << "Credit Limit: " << std::setprecision (2) << std::fixed << creditLimit << std::endl;
            std::cout << "Balance: " << std::setprecision (2) << std::fixed << newBalance << std::endl;
            std::cout << "Credit Limit Exceeded. " << std::endl;
        }
    }
    return 0;
}

