#include <iostream>

int
main()
{
    int i = 1;

    while (i <= 8) {
        if (i % 2 == 0) {
            std::cout << ' ';
        }
        int space = 1;
        while (space <= 8) {
            std::cout << "* ";
            ++space;
        }
        std::cout << std::endl;
        ++i;
    }
    return 0;
}

