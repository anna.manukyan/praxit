#include <iostream>

int
main()
{
    int i = 2;
    while (true) {
        std::cout << i * 2 << std::endl;
        i += i;
    }
    return 0;
}

