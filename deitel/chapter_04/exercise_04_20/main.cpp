#include <iostream>
#include "analysis.hpp"

int
main()
{
    Analysis application;
    return application.processExamResults();
}
