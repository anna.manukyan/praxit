#include "analysis.hpp"
#include <iostream>
#include <unistd.h>

int
Analysis::processExamResults()
{
    int passes = 0;
    int failures = 0;
    int counter = 1;

    while (counter <= 10) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter result (1 = pass, 2 = fail): ";
        }
        int result;
        std::cin >> result;

        if (1 == result) {
            ++passes;
        } else if (2 == result) {
            ++failures;
        } else {
            std::cout << "Error 1: You cannot enter a number other then 1 or 2." << std::endl;
            return 1;
        }

        ++counter;
    }

    std::cout << "Passed " << passes << "\nFailed " << failures << std::endl;
    if (passes > 7) {
        std::cout << "Raise tuition" << std::endl;
    }

    return 0;
}

