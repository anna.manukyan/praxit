#include <iostream>
#include <iomanip>

int
main()
{
    while (true) {
        std::cout << "Enter sales in dollars (-1 to end): ";
        float sales;
        std::cin >> sales;
        if (static_cast<int>(sales) == -1) {
            std::cout << "Exiting..." << std::endl;
            return 0;
        }
        if (sales < 0) {
            std::cout << "Error 1: Cannot be a negative number." << std::endl;
            return 1;
        }    
        float salary = 200 + (sales * 0.09);
        std::cout << "Salary: " << std::setprecision(2) << std::fixed << salary << std::endl;
    }
    return 0;
}    
